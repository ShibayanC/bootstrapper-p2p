#!/bin/python
import sys
import socket
import os
import time
import getopt

os.system("clear")

f = open('structured-boostrap.log', 'w')
f.close()
f = open('structured-boostrap.log', 'a')

buff = 153660
portNum = 0
nodes = 0

db = {}

def register(ipAddr, portN, uName, f):
	line = str(ipAddr)+" "+str(portN)
	# print "Line: ",line
	key = uName
	Keys = db.keys()
	if uName not in Keys:
		retmsg = "REGOK "+str(uName)+" 0"
		len_retmsg = len(retmsg)
		retMsg = "00"+str(len_retmsg)+" "+str(retmsg)
		db[key] = [line]
	elif uName in Keys:
		flag = 0
		val = db[key]
		k = 0
		for k in range(len(val)):
			if str(line) == str(val[k]):
				flag = 1
		if flag == 0:
			l = len(val)
			Line = ''
			k = 0
			for k in range(l):
				Line = Line +" "+ str(val[k])
			retmsg = "REGOK "+str(uName)+" "+str(l)+str(Line)
			len_retmsg = len(retmsg)
			retMsg = "00"+str(len_retmsg)+" "+str(retmsg)
			db[key].append(line)
        	else:
            		'''9998 val in reply means
            		there is duplication in value
            		for the same the key
            		'''
            		retmsg = "REGOK "+str(uName)+" 9998"
            		len_retmsg = len(retmsg)
            		retMsg = "00"+str(len_retmsg)+" "+str(retmsg)
	else:
        	''' 9999 val in reply means there is 
        	some error in registering
        	'''
		retmsg = "REGOK "+str(uName)+" 9999"
        	len_retmsg = len(retmsg)
        	retMsg = "00"+str(len_retmsg)+" "+str(retmsg)
	f.write("--- [ "+time.strftime("%c")+"] "+str(retMsg)+" --- \n")
	return retMsg

def delIPaddress(ipAddr, portN, uName, f):
	key = uName
	line = str(ipAddr)+" "+str(portN)
	Keys = db.keys()
	if uName in Keys:
		vals = db[key]
		k = 0
		flag = 0
		for k in range(len(vals)):
			if vals[k] == line:
				ind = k
				flag = 1
		if flag == 1:
			del db[key][ind]
			retmsg = "DEL IPADDRESS OK "+str(uName)+" "+str(line)+" 1"
			len_retmsg = len(retmsg)
			retMsg = "00"+str(len_retmsg)+" "+str(retmsg)
		else:
			retmsg = "DEL ADDRESS OK "+str(uName)+" 9998"
			len_retmsg = len(retmsg)
			retMsg = "00"+str(len_retmsg)+" "+str(retmsg)
	elif db.has_key(key) == False:
		retmsg = "DEL ADDRESS OK "+str(uName)+" 9999"
		len_retmsg = len(retmsg)
		retMsg = "00"+str(len_retmsg)+" "+str(retmsg)
	f.write("--- [ "+time.strftime("%c")+"] "+str(retMsg)+" --> Bootstrap Server --- \n")
	return retMsg

def deluname(uName, f):
	key = uName
	Keys = db.keys()
	if key in Keys:
		''' 1 error message means
        	successfully deleted
        	'''
        	del db[key]
        	retmsg = "DEL UNAME OK "+str(uName)+" 1"
        	len_retmsg = len(retmsg)
        	retMsg = "00"+str(len_retmsg)+" "+str(retmsg)
    	elif key not in Keys:
		''' 9998 error message
        	means key not present
        	'''
        	retmsg = "DEL UNAME OK "+str(uName)+" 9999"
        	len_retmsg = len(retmsg)
        	retMsg = "00"+str(len_retmsg)+" "+str(retmsg)
	f.write("--- [ "+time.strftime("%c")+"] "+str(retMsg)+" --- \n")
	return retMsg

def getIPlist(uName, f):
    	key = uName
	Keys = db.keys()
	if key in Keys:
		val = db[key]
		if len(val) == 0:
			Msg = "GET IPLIST OK "+str(uName)+" 9999"
			MsgSize = len(Msg)
			Msgfinal = "00"+str(MsgSize)+" "+str(Msg)
		else:
			Line = ''
			k = 0
			count = 0
        		for k in range(len(val)):
				count += 1
            			Line = Line +" "+str(val[k])
        		Msg = "GET IPLIST OK "+str(uName)+" "+str(count)+str(Line)
        		MsgSize = len(Msg)
        		Msgfinal = "00"+str(MsgSize)+" "+str(Msg)
	if key not in Keys:
		''' 9999 error message
        	means key is not present
        	in db '''
        	Msg = "GET IPLIST OK "+str(uName)+" 9999"
        	MsgSize = len(Msg)
        	Msgfinal = "00"+str(MsgSize)+" "+str(Msg)
    	f.write("--- [ "+time.strftime("%c")+"] "+str(Msgfinal)+" -- \n")
	return Msgfinal

def main(argv):
    	f.write('--- Shibayan: Bootstrap Server for structured P2P networks ---\n')
    	f.write('--- Boostrap Log --- '+time.strftime("%c")+' ---\n')

    	try:
        	opts, args = getopt.getopt(argv,"hp:n:",["ports=","nodes="])
    	except getopt.GetoptError:
        	print 'python bs_unstructured.py -p <Port Number> -n <Num of Nodes>'
        	sys.exit(2)
    	for opt, arg in opts:
        	if opt == '-h':
            		print 'python bs_unstructured.py -p <Port Number> -n <Num of Nodes>'
            		sys.exit(2)
        	elif opt in ("-n", "--nodes"):
            		nodes = arg
        	elif opt in ("-p", "--port"):
            		portNum = arg

    	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    	hostName = socket.gethostname()
    	ip = str(socket.gethostbyname(hostName))
    	portNum = int(portNum)
    	nodes = int(nodes)
    	server_address = (ip, portNum)
    	sock.bind(server_address)
    	f.write('My Address: '+str(server_address)+' \n')
    	sock.listen(nodes)
    	while True:
        	conn, addr = sock.accept()
                print "Conn: "+str(conn)+" Addr: "+str(addr)
        	data = conn.recv(buff)
		data = data.strip()
		words = data.split(" ")
		if data is not None and (len(words)) > 3:
			f.write("--- [ "+time.strftime("%c")+"] "+str(addr)+" --> Bootstrap Server --- \n")
        		f.write("Message: "+str(data)+" \n")
			data = data.strip()
        		words = data.split(" ")
        		word = words[1]

        		if str(word) == "GET":
				if str(words[2]) == "IPLIST":
					wordCount = len(words)
					if wordCount == 4:
						try:
                					uName = words[3]
                    					retmsg_get = getIPlist(uName, f)
                    					conn.send(retmsg_get)
            					except IOError as e:
                					''' -1 Error value
                					means wrong format
                					in input command
                					'''
                					retmsg = "GET IPLIST OK -1"
                					len_retmsg = len(retmsg)
                					retmsg_get = "00"+str(len_retmsg)+" "+str(retmsg)
                					f.write("--- [ "+time.strftime("%c")+"] "+str(retmsg_get)+" --- \n")
                					conn.send(retmsg_get)
					else:
						retmsg = "GET IPLIST OK -9998"
						len_retmsg = len(retmsg)
						retmsg_get = "00"+str(len_retmsg)+" "+str(retmsg)
						f.write("--- ["+ time.strftime("%c")+"] "+str(retmsg_get) +" \n")
						conn.send(retmsg_get)
				else:
					retmsg = "GET OK -9999"
					len_retmsg = len(retmsg)
					retmsg_get = "00"+str(len_retmsg)+" "+str(retmsg)
					f.write("--- ["+ time.strftime("%c")+"] "+str(retmsg_get) +" \n")
					conn.send(retmsg_get)

        		elif str(word) == "REG":
            			wordCount = len(words)
				if wordCount == 5:
					try:
                				ipAddr = str(words[2])
                				portN = str(words[3])
                				uName = str(words[4])
                				retmsg_reg = register(ipAddr, portN, uName, f)
                				conn.send(retmsg_reg)
            				except IOError as e:
                				''' -1 Error value
                				means wrong format
                				in input command
                				'''
                				retmsg = "REGOK -1"
                				len_retmsg = len(retmsg)
                				retmsg_reg = "00"+str(len_retmsg)+" "+str(retmsg)
                				f.write("--- [ "+time.strftime("%c")+"] "+str(retmsg_reg)+" --- \n")
                				conn.send(retmsg_reg)
				else:
					retmsg = "REGOK -1"
					len_retmsg = len(retmsg)
					retmsg_reg = "00"+str(len_retmsg)+" "+str(retmsg)
					f.write("--- [ "+time.strftime("%c")+"] "+str(retmsg_reg)+" --- \n")
					conn.send(retmsg_reg)

        		elif str(word) == "DEL":
            			if str(words[2]) == "IPADDRESS":
					wordCount = len(words)
					if wordCount == 6:
						try:
							ipAddr = str(words[3])
                    					portN = str(words[4])
                    					uName = str(words[5])
                    					retmsg_deliplist = delIPaddress(ipAddr, portN, uName, f)
                    					conn.send(retmsg_deliplist)
                				except IOError as e:
                    					''' -1 Error value
                    					means wrong format
                    					in input command
                    					'''
                    					retmsg = "DEL IPADDRESS OK -1"
                    					len_retmsg = len(retmsg)
                    					retmsg_deliplist = "00"+str(len_retmsg)+" "+str(retmsg)
                    					f.write("--- [ "+time.strftime("%c")+"] "+str(retmsg_deliplist)+" --- \n")
                    					conn.send(retmsg_reg)
					else:
						retmsg = "DEL IPADDRESS OK -1"
						len_retmsg = len(retmsg)
						retmsg_deliplist = "00"+str(len_retmsg)+" "+str(retmsg)
						f.write("--- [ "+time.strftime("%c")+"] "+str(retmsg_deliplist)    +" --- \n")

            			elif str(words[2]) == "UNAME":
					wordCount = len(words)
					if wordCount == 4:
						try:
                    					uName = words[3]
                    					retmsg_deluname = deluname(uName, f)
                    					conn.send(retmsg_deluname)
                				except IOError as e:
                    					''' -1 Error value
                    					means wrong format
                    					in input command
                    					'''
                    					retmsg = "DEL UNAME OK -1"
                    					len_retmsg = len(retmsg)
                    					retmsg_deluname = "00"+str(len_retmsg)+" "+str(retmsg)
                    					f.write("--- [ "+time.strftime("%c")+"] "+str(retmsg_deluname)+" --> Bootstrap Server --- \n")
							conn.send(retmsg_reg)
					else:
						retmsg = "DEL UNAME OK -1"
						len_retmsg = len(retmsg)
						retmsg_deluname = "00"+str(len_retmsg)+" "+str(retmsg)
						f.write("--- [ "+time.strftime("%c")+"] "+str(retmsg_deluname)+    " --> Bootstrap Server --- \n")

            			else:
                			Msg = "0012 DEL OK -1"
                			conn.send(Msg)
			else:
            			Msg = "0072 BS REQ -9998"
            			conn.send(Msg)
		else:
			Msg = "0072 BS REQ -9999"
			conn.send(Msg)
    	return

if __name__ == "__main__":
    main(sys.argv[1:])
