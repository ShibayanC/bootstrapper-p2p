# Unstructured Bootstrapper_P2P
This source code is to be used to act as a Bootstrapper for an unstructured P2P network

to execute:
./unstructuredP2P_bootstrapper -p <port Num> -n <num of nodes in network>
<port Num> is the port where the BS server is listening to
<num of nodes in network> is the total number of nodes present in the network

# Structured P2P _ Bootstrapper

This source code is to be used to act as a Bootstrapper for an structured P2P network

to execute:
./structuredP2P_bootstrapper -p <port Num> -n <num of nodes in network>
<port Num> is the port where the BS server is listening to
<num of nodes in network> is the total number of nodes present in the network
